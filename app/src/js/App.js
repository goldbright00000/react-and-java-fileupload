import React, { Component } from 'react';
import axios from 'axios';
import { Progress } from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import GoogleDocsViewer from 'react-google-docs-viewer';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: null,
      loaded: 0,
      data: [],
      columns: [],
      currentFileUrl: null
    }
  }
  baseUrl = "http://localhost:8080/";
  componentWillMount() {
    this.getFileList();
  }

  getFileList() {
    axios.get(this.baseUrl + "fileList")
      .then(res => {
        this.setState({ data: res.data });
      });
  }

  checkMimeType = (event) => {
    //getting file object
    let files = event.target.files
    //define message container
    let err = []
    // list allow mime type
    // loop access array
    for (var z = 0; z < err.length; z++) {// if message not same old that mean has error 
      // discard selected file
      toast.error(err[z])
      event.target.value = null
    }
    return true;
  }

  renderTableData() {
    return this.state.data.map((fileList, index) => {
      const { id, fileName, fileId } = fileList;
      return (
        <tr key={index}>
          <td>{index}</td>
          <td onClick={() => this.onFileClickHandler(fileId)}>{fileName}</td>
        </tr>
      );
    });
  }

  maxSelectFile = (event) => {
    let files = event.target.files
    if (files.length > 3) {
      const msg = 'Only 3 images can be uploaded at a time'
      event.target.value = null
      toast.warn(msg)
      return false;
    }
    return true;
  }
  checkFileSize = (event) => {
    let files = event.target.files
    let size = 20000000000
    let err = [];
    for (var x = 0; x < files.length; x++) {
      if (files[x].size > size) {
        err[x] = files[x].type + 'is too large, please pick a smaller file\n';
      }
    };
    for (var z = 0; z < err.length; z++) {// if message not same old that mean has error 
      // discard selected file
      toast.error(err[z])
      event.target.value = null
    }
    return true;
  }
  onChangeHandler = event => {
    var files = event.target.files
    if (this.maxSelectFile(event) && this.checkMimeType(event) && this.checkFileSize(event)) {
      // if return true allow to setState
      this.setState({
        selectedFile: files,
        loaded: 0
      })
    }
  }

  onUploadClickHandler = () => {
    if (!this.state.selectedFile || this.state.selectedFile.length < 1)
      return;

    const data = new FormData();
    for (var x = 0; x < this.state.selectedFile.length; x++) {
      data.append('file', this.state.selectedFile[x])
    }
    axios.post(this.baseUrl + "uploadFile", data, {
      onUploadProgress: ProgressEvent => {
        this.setState({
          loaded: (ProgressEvent.loaded / ProgressEvent.total * 100),
        })
      }
    })
      .then(res => {
        toast.success('upload success');
        this.getFileList();
      })
      .catch(err => {
        toast.error('upload fail')
      })
  }

  onFileClickHandler = (fileId) => {
    this.setState({ currentFileUrl: this.baseUrl + "downloadFile/" + fileId });
  }

  render() {
    return (
      <div className="container">
        <h1>File Upload and View</h1>
        <div className="row">
          <div className="col-md-6" id="file_upload">
            <h5>File Upload</h5>
            <div className="form-group files">
              <input type="file" name="file" onChange={this.onChangeHandler} />
            </div>
            <div className="form-group">
              <ToastContainer />
              <Progress max="100" color="success" value={this.state.loaded} >{Math.round(this.state.loaded, 2)}%</Progress>
            </div>
            <button type="button" className="btn btn-success btn-block" onClick={this.onUploadClickHandler}>Upload</button>
          </div>
          <div className="col-md-6" id="file_list">
            <h5>File List</h5>
            <table id="fileList">
              <thead>
                <tr>
                  <th>No</th>
                  <th>File Name</th>
                </tr>
              </thead>
              <tbody>
                {this.renderTableData()}
              </tbody>
            </table>
          </div>
        </div>
        <div className="row preview">
          <h5>Doc Viewer</h5>
          <GoogleDocsViewer
            fileUrl={this.state.currentFileUrl}
            width="500px"
            height="400px"
          />
        </div>
      </div>
    );
  };
}

export default App;