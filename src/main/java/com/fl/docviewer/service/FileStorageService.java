package com.fl.docviewer.service;

import com.fl.docviewer.exception.FileStorageException;
import com.fl.docviewer.exception.MyFileNotFoundException;
import com.fl.docviewer.property.FileStorageProperties;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.fl.docviewer.model.FileStorageRepository;
import com.fl.docviewer.model.FileInfoRepository;
import com.fl.docviewer.model.FileInfo;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Service
@EnableMongoRepositories(basePackageClasses = FileStorageRepository.class)
@EnableJpaRepositories(basePackageClasses = FileInfoRepository.class)
public class FileStorageService {

    private final Path fileStorageLocation;
    public Object uploadMultipleFiles;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
                    ex);
        }
    }

    @Autowired
    private FileInfoRepository fileInfoRepository;

    @Autowired
    GridFsOperations gridFsOps;

    public String storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // save file to GridFS
            DBObject metaData = new BasicDBObject();
            ObjectId fileId = gridFsOps.store(file.getInputStream(), fileName, metaData);

            FileInfo n = new FileInfo();
            n.setFileName(fileName);
            n.setFileId(fileId.toString());

            fileInfoRepository.save(n);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileId) {
        Resource resource = gridFsOps.getResource(gridFsOps.findOne(new Query(Criteria.where("_id").is(fileId))));
        if (resource.exists()) {
            return resource;
        } else {
            throw new MyFileNotFoundException("File not found " + fileId);
        }
    }

    public List<FileInfo> getFileList() {
        List<FileInfo> result = new ArrayList<FileInfo>();
        fileInfoRepository.findAll().forEach(result::add);
        return result;
    }
}