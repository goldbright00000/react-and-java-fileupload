package com.fl.docviewer;

import com.fl.docviewer.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
public class DocviwerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocviwerApplication.class, args);
	}

}
