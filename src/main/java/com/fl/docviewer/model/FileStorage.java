package com.fl.docviewer.model;

import org.springframework.data.annotation.Id;

public class FileStorage {

    @Id
    public String id;

    public String firstName;
    public String lastName;

    public FileStorage() {
    }

    public FileStorage(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return String.format("Customer[id=%s, firstName='%s', lastName='%s']", id, firstName, lastName);
    }

}