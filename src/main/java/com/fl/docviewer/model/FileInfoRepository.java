package com.fl.docviewer.model;

import org.springframework.data.repository.CrudRepository;

import com.fl.docviewer.model.FileInfo;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface FileInfoRepository extends CrudRepository<FileInfo, Integer> {

}