package com.fl.docviewer.model;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fl.docviewer.model.FileStorage;

public interface FileStorageRepository extends MongoRepository<FileStorage, String> {

    public FileStorage findByFirstName(String firstName);

    public List<FileStorage> findByLastName(String lastName);

}